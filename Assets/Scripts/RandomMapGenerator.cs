﻿// This script is placed in public domain. The author takes no responsibility for any possible harm.

/* var heightMap : Texture2D;
var material : Material;
var size = Vector3(200, 30, 200);

function Start ()
{
	GenerateHeightmap();
}

function GenerateHeightmap ()
{
	// Create the game object containing the renderer
	gameObject.AddComponent(MeshFilter);
	gameObject.AddComponent("MeshRenderer");
	if (material)
		renderer.material = material;
	else
		renderer.material.color = Color.white;

	// Retrieve a mesh instance
	var mesh : Mesh = GetComponent(MeshFilter).mesh;

	var width : int = Mathf.Min(heightMap.width, 255);
	var height : int = Mathf.Min(heightMap.height, 255);
	var y = 0;
	var x = 0;

	// Build vertices and UVs
	var vertices = new Vector3[height * width];
	var uv = new Vector2[height * width];
	var tangents = new Vector4[height * width];
	
	var uvScale = Vector2 (1.0 / (width - 1), 1.0 / (height - 1));
	var sizeScale = Vector3 (size.x / (width - 1), size.y, size.z / (height - 1));
	
	for (y=0;y<height;y++)
	{
		for (x=0;x<width;x++)
		{
			var pixelHeight = heightMap.GetPixel(x, y).grayscale;
			var vertex = Vector3 (x, pixelHeight, y);
			vertices[y*width + x] = Vector3.Scale(sizeScale, vertex);
			uv[y*width + x] = Vector2.Scale(Vector2 (x, y), uvScale);

			// Calculate tangent vector: a vector that goes from previous vertex
			// to next along X direction. We need tangents if we intend to
			// use bumpmap shaders on the mesh.
			var vertexL = Vector3( x-1, heightMap.GetPixel(x-1, y).grayscale, y );
			var vertexR = Vector3( x+1, heightMap.GetPixel(x+1, y).grayscale, y );
			var tan = Vector3.Scale( sizeScale, vertexR - vertexL ).normalized;
			tangents[y*width + x] = Vector4( tan.x, tan.y, tan.z, -1.0 );
		}
	}
	
	// Assign them to the mesh
	mesh.vertices = vertices;
	mesh.uv = uv;

	// Build triangle indices: 3 indices into vertex array for each triangle
	var triangles = new int[(height - 1) * (width - 1) * 6];
	var index = 0;
	for (y=0;y<height-1;y++)
	{
		for (x=0;x<width-1;x++)
		{
			// For each grid cell output two triangles
			triangles[index++] = (y     * width) + x;
			triangles[index++] = ((y+1) * width) + x;
			triangles[index++] = (y     * width) + x + 1;

			triangles[index++] = ((y+1) * width) + x;
			triangles[index++] = ((y+1) * width) + x + 1;
			triangles[index++] = (y     * width) + x + 1;
		}
	}
	// And assign them to the mesh
	mesh.triangles = triangles;
		
	// Auto-calculate vertex normals from the mesh
	mesh.RecalculateNormals();
	
	// Assign tangents after recalculating normals
	mesh.tangents = tangents;
} */

/* using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMapGenerator : MonoBehaviour {

	[SerializeField]
	private float heightMultiplier;
	
	[SerializeField]
	private MeshFilter meshFilter;
	
	[SerializeField]
	private MeshCollider meshCollider;
	
	[SerializeField]
	private float mapScale;
	
	private Vector3[] meshVertices;

	void Start() {
		// calculate the offsets based on the tile position
		
		int width = 10, height = 10;
		meshVertices = new Vector3[width * height * 2];
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				meshVertices[x * width + y] = new Vector3(x, y, 0.0f);
			}
		}
		
		int tileDepth = (int)Mathf.Sqrt (meshVertices.Length);
		int tileWidth = tileDepth;
		
		float[,] heightMap = GenerateNoiseMap(tileDepth, tileWidth, this.mapScale);
		
		UpdateMeshVertices(heightMap);
	}
	
	public float[,] GenerateNoiseMap(int mapDepth, int mapWidth, float scale) {
                // create an empty noise map with the mapDepth and mapWidth coordinates
		float[,] noiseMap = new float[mapDepth, mapWidth];
 
		for (int zIndex = 0; zIndex < mapDepth; zIndex ++) {
			for (int xIndex = 0; xIndex < mapWidth; xIndex++) {
                                // calculate sample indices based on the coordinates and the scale
				float sampleX = xIndex / scale;
				float sampleZ = zIndex / scale;
 
                                // generate noise value using PerlinNoise
				float noise = Mathf.PerlinNoise (sampleX, sampleZ);
 
				noiseMap [zIndex, xIndex] = noise;
			}
		}
 
		return noiseMap;
	}

	private void UpdateMeshVertices(float[,] heightMap) {
		int tileDepth = heightMap.GetLength (0);
		int tileWidth = heightMap.GetLength (1);

		// iterate through all the heightMap coordinates, updating the vertex index
		int vertexIndex = 0;
		for (int zIndex = 0; zIndex < tileDepth; zIndex++) {
			for (int xIndex = 0; xIndex < tileWidth; xIndex++) {
				float height = heightMap [zIndex, xIndex];

				Vector3 vertex = meshVertices [vertexIndex];
				// change the vertex Y coordinate, proportional to the height value
				meshVertices[vertexIndex] = new Vector3(vertex.x, height * this.heightMultiplier, vertex.z);

				vertexIndex++;
			}
		}

		// update the vertices in the mesh and update its properties
		this.meshFilter.mesh.vertices = meshVertices;
		this.meshFilter.mesh.RecalculateBounds ();
		this.meshFilter.mesh.RecalculateNormals ();
		// update the mesh collider
		this.meshCollider.sharedMesh = this.meshFilter.mesh;
	}
} */