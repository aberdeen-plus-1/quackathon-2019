﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhaleBus : MonoBehaviour {
	public GameObject enemy;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().velocity += new Vector3(0, 0, 1f);

		InvokeRepeating("Spawn", 1f, 1f);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F))
        {
			// Drop the player!
            GameObject player = GameObject.Find("Player");
			if (player) {
				player.transform.parent = transform.parent;
				GameObject.Find("PlayerShadow").GetComponent<Rigidbody>().isKinematic = false;
				GameObject.Find("WhaleCam").GetComponent<Camera>().enabled = false;
				GameObject.Find("PlayerCam").GetComponent<Camera>().enabled = true;
			}
        }
	}

	void Spawn ()
	{
		if (Random.Range(0, 100) < 66) return;
		Vector3 position = GameObject.Find("WhaleBus").transform.position;
		// Quaternion rotation = new Quaternion(-90f,0f,0f, 0f);
		GameObject player = GameObject.Find("Player");
		GameObject newEnemy = Instantiate(enemy, position, player.transform.rotation);
	  newEnemy.transform.parent = GameObject.Find("WhaleBus").transform;
	}

}
